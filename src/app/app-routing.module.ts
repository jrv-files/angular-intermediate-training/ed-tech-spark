import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//eager loading
import { PublicModule } from './modules/public/public.module';
import { AuthUserGuard } from './auth-user.guard';
import { AuthAdminGuard } from './auth-admin.guard';

const routes: Routes = [
  //lazy loading
  { path: 'user', canMatch: [AuthUserGuard], loadChildren: () => import('./modules/user/user.module').then(m => m.UserModule) },
  { path: 'admin', canMatch: [AuthAdminGuard], loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule) },
  //eager loading
  { path: '', loadChildren: () => PublicModule }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
