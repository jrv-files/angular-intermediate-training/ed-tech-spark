import { Component, OnInit } from '@angular/core';
import { Cart } from 'src/app/models/cart';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styles: [
  ]
})
export class CartComponent implements OnInit {
  cart: Cart;
  constructor(private cartService: CartService) {
    this.cart = this.cartService.GetCart();
  }

  ngOnInit(): void {
  }

}
