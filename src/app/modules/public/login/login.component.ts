import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {
  userForm: FormGroup;
  message:string | undefined;
  constructor(private fb: FormBuilder, private authService: AuthService, private router:Router) {
    this.userForm = this.fb.group({
      username: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]]
    });
  }

  ngOnInit(): void {
  }
  LoginUser() {
    if (this.userForm.valid) {
      this.authService.ValidateUser(this.userForm.value).subscribe(res => {
        if (res.status == 200 && res.body != null) {
          const user = res.body;
          this.authService.SetAuthUser(user);
          if (user.roles.find(role => role == 'Admin') == 'Admin') {
            this.router.navigate(['/admin']);
          }
          else if (user.roles.find(role => role == 'User') == 'User') {
            this.router.navigate(['/user']);
          }
        }
        else{
          this.message = 'Invalid username or password';
        }
      });
    }
  }
}
